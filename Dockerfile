FROM alpine:3.13

ENV PACKAGES "curl nano postgresql-client mysql-client"

RUN apk add --no-cache $PACKAGES
